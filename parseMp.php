<?php


$hostname = "localhost";
$dbname = "mp_data";
$username = "portal";
$password = "!QJUq#wrp0a3";


$db = new PDO('mysql:host='.$hostname.';dbname='.$dbname.';charset=utf8mb4', $username, $password);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

// adjust these based on available market cycles
$market_cycles = array(
	'Winter 2016' => 'w16',
	'Summer 2016' => 's16',
	'Winter 2017' => 'w17',
	'Summer 2017' => 's17',
	'Winter 2018' => 'w18'
);

$market_cycles_flipped = array_flip($market_cycles);

$profile_columns = array(
	'user_id',
	'first_name',
	'last_name',
	'email',
	'product_category_of_interest',
	'created_on',
	'market',
	'qty_of_lists'
);


$data_columns_init = array(
	'qty_of_lists_',
	'qty_of_exhibitor_items_',
	'qty_of_event_items_',
	'list_exhibitor_ids_',
	'list_exhibitor_names_',
	'list_exhibitor_category_names_',
	'list_exhibitor_styles_',
	'list_exhibitor_price_points_',
	'qty_exhibitors_location_a_',
	'qty_exhibitors_location_b_',
	'qty_exhibitors_location_c_',
	'qty_exhibitors_location_p_'
);

$data_columns = array();


// for($x = 0; $x < count($market_cycles); $x++) {
while(list($k,$v) = each($market_cycles)) {
	$current = $v;

	foreach($data_columns_init as $m) {
		$data_columns[] = $m.$current;	
	}
}
$header_columns = array_merge($profile_columns,$data_columns);
$number_columns = count($header_columns);

// get csv file (stub for s3 grab later)
//
//

$row = 1;
// read in csv file
if (($handle = fopen("../csv/mpdata.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 40960, ",")) !== FALSE) {
        $num = count($data);

        if($row > 1) { // skip the header row
	        if($num == $number_columns) {
		        $tmp = array();
		        $user_id = trim($data[0]);
		        for ($c=0; $c < $num; $c++) {
		            $column_name = $header_columns[$c];
		            $tmp[$column_name] = trim($data[$c]);
		        }
		        $out[] = $tmp;
	        }
	        else {
		        echo $row." contains invalid number of columns: ".$num."\n";
	        }
	}
        $row++;



    }
    fclose($handle);
}

if(count($out) > 0) {
	$sql = "delete from mp_user_profile";
	$stmt = $db->prepare($sql);
	$stmt->execute();

	$sql = "delete from mp_user_market_data";
	$stmt = $db->prepare($sql);
	$stmt->execute();

}

foreach($out as $d) {
	$user_id = $d["user_id"];
	$first_name = $d["first_name"];
	$last_name = $d["last_name"];
	$email = $d["email"];
	$product_category_of_interest = $d["product_category_of_interest"];
	$total_lists = $d["qty_of_lists"];

	$sql = "insert into mp_user_profile(user_id,first_name,last_name,email,product_category_of_interest,qty_of_lists) values(?,?,?,?,?,?)";

	$stmt = $db->prepare($sql);
	$stmt->execute(array($user_id,$first_name,$last_name,$email,$product_category_of_interest,$total_lists));
	$line_cnt = $stmt->rowCount();

	$tmp = array();
	$build = array();
	while(list($column_name,$v) = each($d)) {

		if(strstr($column_name,"category_names")) {

			$parts = explode("_",$column_name);
			$market_part = $parts[count($parts) -1];

			$categories = explode("|",trim($v));

			$sql = "delete from mp_user_categories where user_id = ? and market = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($user_id,$market_part));

			if(count($categories) > 0) {
			    	foreach($categories as $category) {
			    		if(strlen($category) > 0) {
			            		$sql = "insert into mp_user_categories(user_id,market,category_name) values(?,?,?)";
			            		$stmt = $db->prepare($sql);
			            		$stmt->execute(array($user_id,$market_part,$category));
			            	}
			    	}
			}
		}
		elseif($column_name == 'market') { // markets
			$sql = "delete from mp_user_markets where user_id = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($user_id));

			$markets = explode(",",trim($v));
			if(count($markets) > 0) {	
		    		foreach($markets as $m) {
		    			$use_market = $market_cycles[trim($m)];
		    			$sql = "insert into mp_user_markets(user_id,market) values(?,?)";
		    			$stmt = $db->prepare($sql);
		    			$stmt->execute(array($user_id,$use_market));
		    		}
			}
		}
		elseif(strstr($column_name,"styles")) {

			$parts = explode("_",$column_name);
			$market_part = $parts[count($parts) -1];

			$styles = explode("|",trim($v));

			$sql = "delete from mp_user_styles where user_id = ? and market = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($user_id,$market_part));

			if(count($styles) > 0) {
			    	foreach($styles as $style) {
			    		if(strlen($style) > 0) {
			            		$sql = "insert into mp_user_styles(user_id,market,style) values(?,?,?)";
			            		$stmt = $db->prepare($sql);
			            		$stmt->execute(array($user_id,$market_part,$style));
			            	}
			    	}
			}

		}
		elseif(strstr($column_name,"price_points")) {
			$parts = explode("_",$column_name);
			$market_part = $parts[count($parts) -1];

			$price_points = explode("|",trim($v));

			$sql = "delete from mp_user_price_points where user_id = ? and market = ?";
			$stmt = $db->prepare($sql);
			$stmt->execute(array($user_id,$market_part));

			if(count($price_points) > 0) {
			    	foreach($price_points as $price_point) {
			    		if(strlen($price_point) > 0) {
			            		$sql = "insert into mp_user_price_points(user_id,market,price_point) values(?,?,?)";
			            		$stmt = $db->prepare($sql);
			            		$stmt->execute(array($user_id,$market_part,$price_point));
			            	}
			    	}
			}
		}
		else {
			$parts = explode("_",$column_name);
			$market_part = $parts[count($parts) -1];
			if(isset($market_cycles_flipped[$market_part])) {
				$new_column_name = str_replace("_".$market_part,"",$column_name);
				if(!(isset($build[$market_part]))) {
					$build[$market_part] = array();
				}
				$build[$market_part]["user_id"] = $user_id;
				if(substr($new_column_name,0,2) === 'qty') {
					if(is_numeric($v)) {
						$build[$market_part][$new_column_name] = $v;
					}
					else {
						$build[$market_part][$new_column_name] = 0;
					}
				}
				else {
					$build[$market_part][$new_column_name] = trim($v);
				}
			}
		}
	}

	while(list($k,$v) = each($build)) {
		$use_market = trim($k);
		$sql = "insert into mp_user_market_data(user_id,market,qty_of_lists,qty_of_exhibitor_items,qty_of_event_items,list_exhibitor_ids,list_exhibitor_names,qty_exhibitors_location_a,qty_exhibitors_location_b,qty_exhibitors_location_c,qty_exhibitors_location_p) values(?,?,?,?,?,?,?,?,?,?,?)";
		$stmt = $db->prepare($sql);
		$stmt->execute(array(
				$v["user_id"],
				$use_market,
				$v["qty_of_lists"],
				$v["qty_of_exhibitor_items"],
				$v["qty_of_event_items"],
				$v["list_exhibitor_ids"],
				$v["list_exhibitor_names"],
				$v["qty_exhibitors_location_a"],
				$v["qty_exhibitors_location_b"],
				$v["qty_exhibitors_location_c"],
				$v["qty_exhibitors_location_p"]
			)
		);	
	}
}
echo $number_columns." columns\n";

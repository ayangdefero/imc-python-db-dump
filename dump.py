import MySQLdb
import csv
import json
# Open database connection
# db = MySQLdb.connect("localhost","root","","imc-attendee" )
db = MySQLdb.connect("localhost","portal","!QJUq#wrp0a3","imc_attendee_prod" )

marketIdsList = [1,2,3,4,5]
marketCycles = ["w16", "s16" , "w17", "s17", "w18"]
# prepare a cursor object using cursor() method
cursor = db.cursor()

# execute SQL query using execute() method.
query = """
SELECT
 a.id as user_id,
 a.firstName as first_name,
 a.lastName as last_name,
 a.user as email,
 (case `a`.`productCategoryOfInterest` when 1 then 'Furniture' when 2 then 'Home Decor' when 3 then 'Gift' else 'n/z' end) AS `product_category_of_interest`,
 a.`mp_category` as mp_category,
 a.createdDatetime as created_on,
 group_concat(distinct (case `b`.`market_id` when 1 then 'Winter 2016' when 2 then 'Summer 2016' when 3 then 'Winter 2017' when 4 then 'Summer 2017' when 5 then 'Winter 2018' else 'n/a' end) separator ',') AS `market`,
 count(b.attendee_id) as qty_of_lists
 FROM `imc_attendee_prod`.attendees a
LEFT JOIN `imc_attendee_prod`.attendee_favorite_groups b on a.id =b.attendee_id
group by `a`.`id`
order by `a`.`createdDatetime` desc;
"""
allColumns = []
cursor.execute(query)
firstResult = {}
columns = cursor.description
columns = columns[1:]
allColumns.append([col[0] for col in columns])
tempResult = cursor.fetchall()
for row in tempResult:
    # firstResult[row[0]] = {columns[index][0]:column for index, column in enumerate(row[1:])}
    firstResult[row[0]] = list(row[1:])

exhibitorItemsCount = {}
columnsAdded = 0

cursor.execute("SET group_concat_max_len=30000;"); #SET sql_mode = ''")
for key in firstResult.keys():
    for marketNum, marketId in enumerate(marketIdsList):
        # execute SQL query using execute() method.
        query = """
        SELECT
         a.id as user_id,
         count(*) as %s
         FROM `imc_attendee_prod`.attendees a
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_groups g on a.id =g.attendee_id
        WHERE g.market_id = %s
        AND a.id = %s
        group by `a`.`id`
        """ % ("qty_of_lists_" + marketCycles[marketNum], marketId, key)

        cursor.execute(query)

        columns = cursor.description

        columns = columns[1:]

        if not columnsAdded:
            allColumns[0] += [col[0] for col in columns]

        tempResult = cursor.fetchall()

        secondResult = {}
        if tempResult:
            row = tempResult[0]
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + list(row[1:])
                else:
                    exhibitorItemsCount[key][marketId] = list(row[1:])
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = list(row[1:])
        else:
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + [""]
                else:
                    exhibitorItemsCount[key][marketId] = [""]
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = [""]

        # execute SQL query using execute() method.
        query = """
        SELECT
         a.id as user_id,
         count(*) as %s
         FROM `imc_attendee_prod`.attendee_favorite_groups_favorites fgf
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_groups g on fgf.group_id =g.id
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_favorites f on fgf.favorite_id = f.id
        LEFT JOIN `imc_attendee_prod`.attendees a on g.attendee_id = a.id
        WHERE f.favoritable_type = "exhibitor"
        AND g.market_id = %s
        AND a.id = %s
        group by `a`.`id`
        """ % ("qty_of_exhibitor_items_" + marketCycles[marketNum], marketId, key)

        cursor.execute(query)

        columns = cursor.description

        columns = columns[1:]

        if not columnsAdded:
            allColumns[0] += [col[0] for col in columns]

        tempResult = cursor.fetchall()

        secondResult = {}
        if tempResult:
            row = tempResult[0]
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + list(row[1:])
                else:
                    exhibitorItemsCount[key][marketId] = list(row[1:])
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = list(row[1:])
        else:
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + [""]
                else:
                    exhibitorItemsCount[key][marketId] = [""]
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = [""]

        # execute SQL query using execute() method.
        query = """
        SELECT
         a.id as user_id,
         count(*) as %s
         FROM `imc_attendee_prod`.attendee_favorite_groups_favorites fgf
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_groups g on fgf.group_id =g.id
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_favorites f on fgf.favorite_id = f.id
        LEFT JOIN `imc_attendee_prod`.attendees a on g.attendee_id = a.id
        WHERE f.favoritable_type = "event"
        AND g.market_id = %s
        AND a.id = %s
        group by `a`.`id`
        """ % ("qty_of_event_items_" + marketCycles[marketNum], marketId, key)

        cursor.execute(query)

        columns = cursor.description
        columns = columns[1:]

        if not columnsAdded:
            allColumns[0] += [col[0] for col in columns]
        tempResult = cursor.fetchall()

        if tempResult:
            row = tempResult[0]
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + list(row[1:])
                else:
                    exhibitorItemsCount[key][marketId] = list(row[1:])
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = list(row[1:])
        else:
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + [""]
                else:
                    exhibitorItemsCount[key][marketId] = [""]
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = [""]

        #execute SQL query using execute() method.
        query = """
        SELECT
         a.id as user_id,
         GROUP_CONCAT(DISTINCT f.favoritable_identifier SEPARATOR '|') as %s,
         GROUP_CONCAT(DISTINCT f.favoritable_name SEPARATOR '|') as %s,
         GROUP_CONCAT(DISTINCT spc.label SEPARATOR '|') as %s,

         GROUP_CONCAT(DISTINCT cstyles.label SEPARATOR '|') as %s,
         GROUP_CONCAT(DISTINCT cpp.label SEPARATOR '|') as %s

         FROM `imc_attendee_prod`.attendee_favorite_groups_favorites fgf
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_groups g on fgf.group_id =g.id
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_favorites f on fgf.favorite_id = f.id
        LEFT JOIN `imc_exhibitor`.company_product_categories_offered cpco on f.favoritable_identifier = cpco.company_id
        LEFT JOIN `imc_exhibitor`.system_product_categories spc on cpco.system_product_category_id = spc.product_category_id

        LEFT JOIN `imc_exhibitor`.company_styles_view cstyles on cstyles.company_id = f.favoritable_identifier
        LEFT JOIN `imc_exhibitor`.company_price_points_view cpp on cpp.company_id = f.favoritable_identifier

        LEFT JOIN `imc_attendee_prod`.attendees a on g.attendee_id = a.id
        WHERE f.favoritable_type = "exhibitor"
        AND g.market_id = %s
        AND a.id = %s
        group by `a`.`id`
        """ % ("list_exhibitor_ids_" + marketCycles[marketNum],
         "list_exhibitor_names_" + marketCycles[marketNum],
         "list_exhibitor_category_names_" + marketCycles[marketNum],
         "list_exhibitor_styles_" + marketCycles[marketNum],
         "list_exhibitor_price_points_" + marketCycles[marketNum],
          marketId,
          key)

        cursor.execute(query)

        columns = cursor.description

        columns = columns[1:]

        if not columnsAdded:
            allColumns[0] += [col[0] for col in columns]
        tempResult = cursor.fetchall()

        if tempResult:
            row = tempResult[0]
            #filter out the identifiers if longer than 4
            tempIdentifier = row[1]
            tempIdentifier = tempIdentifier.split(', ')
            for tempKey, tempId in enumerate(tempIdentifier):
                if len(tempId) > 4:
                    tempSplit = tempId.split("121")
                    tempIdentifier[tempKey] = tempSplit[0]
            res = list(row[1:])
            res[0] = ", ".join(tempIdentifier)
            row = res
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + list(row)
                else:
                    exhibitorItemsCount[key][marketId] = list(row)
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = list(row)
        else:
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + ["", "" ,"","",""]
                else:
                    exhibitorItemsCount[key][marketId] = ["", "","","",""]
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = [""]

        #execute SQL query using execute() method.
        query = """
        SELECT
         a.id as user_id,
         (LENGTH(ca.company_agreement_space_numbers) - LENGTH(REPLACE(ca.company_agreement_space_numbers, ',', '')) + 1) as %s
         FROM `imc_attendee_prod`.attendee_favorite_groups_favorites fgf
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_groups g on fgf.group_id =g.id
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_favorites f on fgf.favorite_id = f.id
        LEFT JOIN `imc_exhibitor`.company_product_categories_offered cpco on f.favoritable_identifier = cpco.company_id
        LEFT JOIN `imc_exhibitor`.system_product_categories spc on cpco.system_product_category_id = spc.product_category_id
        LEFT JOIN `imc_exhibitor`.company_agreements ca on cpco.company_id = ca.company_id
        LEFT JOIN `imc_attendee_prod`.attendees a on g.attendee_id = a.id
        WHERE ca.company_agreement_building = "Building A"
        AND g.market_id = %s
        AND a.id = %s
        GROUP BY f.favoritable_identifier
        """ % ("qty_exhibitors_location_a_" + marketCycles[marketNum], marketId, key)

        cursor.execute(query)

        columns = cursor.description

        columns = columns[1:]


        if not columnsAdded:
            allColumns[0] += [col[0] for col in columns]
        tempResult = cursor.fetchall()

        if tempResult:
            buildingCount = len(tempResult)
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + [buildingCount]
                else:
                    exhibitorItemsCount[key][marketId] = [buildingCount]
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = [buildingCount]
        else:
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + [""]
                else:
                    exhibitorItemsCount[key][marketId] = [""]
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = [""]

        #execute SQL query using execute() method.
        query = """
        SELECT
         a.id as user_id,
         (LENGTH(ca.company_agreement_space_numbers) - LENGTH(REPLACE(ca.company_agreement_space_numbers, ',', '')) + 1) as %s
         FROM `imc_attendee_prod`.attendee_favorite_groups_favorites fgf
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_groups g on fgf.group_id =g.id
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_favorites f on fgf.favorite_id = f.id
        LEFT JOIN `imc_exhibitor`.company_product_categories_offered cpco on f.favoritable_identifier = cpco.company_id
        LEFT JOIN `imc_exhibitor`.system_product_categories spc on cpco.system_product_category_id = spc.product_category_id
        LEFT JOIN `imc_exhibitor`.company_agreements ca on cpco.company_id = ca.company_id
        LEFT JOIN `imc_attendee_prod`.attendees a on g.attendee_id = a.id
        WHERE ca.company_agreement_building = "Building B"
        AND g.market_id = %s
        AND a.id = %s
        GROUP BY f.favoritable_identifier
        """ % ("qty_exhibitors_location_b_" + marketCycles[marketNum], marketId, key)

        cursor.execute(query)

        columns = cursor.description

        columns = columns[1:]


        if not columnsAdded:
            allColumns[0] += [col[0] for col in columns]
        tempResult = cursor.fetchall()

        if tempResult:
            buildingCount = len(tempResult)
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + [buildingCount]
                else:
                    exhibitorItemsCount[key][marketId] = [buildingCount]
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = [buildingCount]
        else:
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + [""]
                else:
                    exhibitorItemsCount[key][marketId] = [""]
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = [""]

        #execute SQL query using execute() method.
        query = """
        SELECT
         a.id as user_id,
         (LENGTH(ca.company_agreement_space_numbers) - LENGTH(REPLACE(ca.company_agreement_space_numbers, ',', '')) + 1) as %s
         FROM `imc_attendee_prod`.attendee_favorite_groups_favorites fgf
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_groups g on fgf.group_id =g.id
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_favorites f on fgf.favorite_id = f.id
        LEFT JOIN `imc_exhibitor`.company_product_categories_offered cpco on f.favoritable_identifier = cpco.company_id
        LEFT JOIN `imc_exhibitor`.system_product_categories spc on cpco.system_product_category_id = spc.product_category_id
        LEFT JOIN `imc_exhibitor`.company_agreements ca on cpco.company_id = ca.company_id
        LEFT JOIN `imc_attendee_prod`.attendees a on g.attendee_id = a.id
        WHERE ca.company_agreement_building = "Building C"
        AND g.market_id = %s
        AND a.id = %s
        GROUP BY f.favoritable_identifier
        """ % ("qty_exhibitors_location_c_" + marketCycles[marketNum], marketId, key)

        cursor.execute(query)

        columns = cursor.description

        columns = columns[1:]


        if not columnsAdded:
            allColumns[0] += [col[0] for col in columns]
        tempResult = cursor.fetchall()
        if tempResult:
            buildingCount = len(tempResult)
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + [buildingCount]
                else:
                    exhibitorItemsCount[key][marketId] = [buildingCount]
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = [buildingCount]
        else:
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + [""]
                else:
                    exhibitorItemsCount[key][marketId] = [""]
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = [""]

        #execute SQL query using execute() method.
        query = """
        SELECT
         a.id as user_id,
         (LENGTH(ca.company_agreement_space_numbers) - LENGTH(REPLACE(ca.company_agreement_space_numbers, ',', '')) + 1) as %s
         FROM `imc_attendee_prod`.attendee_favorite_groups_favorites fgf
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_groups g on fgf.group_id =g.id
        LEFT JOIN `imc_attendee_prod`.attendee_favorite_favorites f on fgf.favorite_id = f.id
        LEFT JOIN `imc_exhibitor`.company_product_categories_offered cpco on f.favoritable_identifier = cpco.company_id
        LEFT JOIN `imc_exhibitor`.system_product_categories spc on cpco.system_product_category_id = spc.product_category_id
        LEFT JOIN `imc_exhibitor`.company_agreements ca on cpco.company_id = ca.company_id
        LEFT JOIN `imc_attendee_prod`.attendees a on g.attendee_id = a.id
        WHERE ca.company_agreement_building LIKE '%s'
        AND g.market_id = %s
        AND a.id = %s
        GROUP BY f.favoritable_identifier
        """ % ("qty_exhibitors_location_p_"+ marketCycles[marketNum],"Pavilion", marketId, key)

        cursor.execute(query)

        columns = cursor.description

        columns = columns[1:]

        if not columnsAdded:
            allColumns[0] += [col[0] for col in columns]
        tempResult = cursor.fetchall()

        if tempResult:
            buildingCount = len(tempResult)
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + [buildingCount]
                else:
                    exhibitorItemsCount[key][marketId] = [buildingCount]
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = [buildingCount]
        else:
            if exhibitorItemsCount.get(key):
                if exhibitorItemsCount.get(key).get(marketId):
                    exhibitorItemsCount[key][marketId] = exhibitorItemsCount.get(key).get(marketId) + [""]
                else:
                    exhibitorItemsCount[key][marketId] = [""]
            else:
                exhibitorItemsCount[key] = {}
                exhibitorItemsCount[key][marketId] = [""]

    # print(key)
    columnsAdded = 1
d = [[key] +
firstResult.get(key, [""] * 5) +
[val for sublist in [exhibitorItemsCount.get(key).get(market) for market in marketIdsList] for val in sublist]
for key in firstResult.keys()]

allColumns[0] = ["user_id"] + allColumns[0]
with open('../csv/mpdata.csv', 'w') as f:
    writer = csv.writer(f, delimiter=",")
    writer.writerow(allColumns[0])
    for row in d:
        writer.writerow(row)


# disconnect from server
db.close()

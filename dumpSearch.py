import MySQLdb
import csv
import json
# Open database connection
# db = MySQLdb.connect("localhost","root","","imc-attendee" )
db = MySQLdb.connect("localhost","portal","!QJUq#wrp0a3","imc_attendee_prod" )

marketIdsList = [1,2,3,4]
# prepare a cursor object using cursor() method
cursor = db.cursor()

# execute SQL query using execute() method.
# imc-attendee, imc_attendee_prod on server
query = """
SELECT
 a.id as user_id,
 c.id as search_id,
 a.firstName as first_name,
 a.lastName as last_name,
 c.name as name,
 c.data as json
FROM `imc_attendee_prod`.attendee_searches b
LEFT JOIN `imc_attendee_prod`.attendees a on a.id = b.userprovidingattendee_id
LEFT JOIN AttendeeSearch c on b.attendee_search_id = c.id
"""
searchColumns = ["user_id", "first_name", "last_name", "search_name", "kw", "exact", "styles","sort_dir"
,"price_points", "sort_col", "locations", "search_per_page", "exhibitor-features", "search_alpha", "search_goto_page", "categories"]
cursor.execute(query)
searchResult = {}
columns = cursor.description
# columns = columns[1:]
tempResult = cursor.fetchall()
for row in tempResult:
    # firstResult[row[0]] = {columns[index][0]:column for index, column in enumerate(row[1:])}
    searchResult[str(row[0]) + "." + str(row[1])] = list(row[2:])

transformedData = []
for result in sorted(searchResult.iterkeys()):
    payload = searchResult.get(result)
    if payload[3] is not None:
        jsonString = json.loads(payload[3])
        transformedPayload = [result, payload[0], payload[1], payload[2], jsonString.get("kw"), jsonString.get("exact")]
        for filterResult in jsonString.get("applied_filters"):
            if filterResult == "sort_dir" or filterResult == "sort_col":
                continue
            elif filterResult == "categories":
                categoriesJson = jsonString.get("applied_filters").get("categories")
                if categoriesJson and len(categoriesJson) > 0:
                    categoriesString = ""
                    for i, category in enumerate(categoriesJson):
                        subCategoriesJson = jsonString.get("applied_filters").get("categories").get(category)
                        #parent id for name lookup in exhibitor table
                        categoryNum = category.split("_")[1]
                        if subCategoriesJson and len(subCategoriesJson) > 0:
                            for i, subcategory in enumerate(subCategoriesJson):
                                subCategoryNum = subcategory
                                # execute SQL query using execute() method.
                                # imc-attendee, imc_attendee_prod on server
                                if categoryNum == subCategoryNum:
                                    query = """
                                    SELECT
                                     a.label as label
                                    FROM `imc_exhibitor`.system_product_categories a
                                    WHERE a.`product_category_id` = %s
                                    """ % (categoryNum)
                                    cursor.execute(query)
                                    columns = cursor.description
                                    tempResult = cursor.fetchall()
                                    if tempResult:
                                        label = tempResult[0][0]
                                    else:
                                        label = "pc_" + categoryNum
                                else:
                                    query = """
                                    SELECT
                                     a.sub_label as label
                                    FROM `imc_exhibitor`.system_product_categories a
                                    WHERE a.`product_category_parent_id` = %s and
                                    a.`product_category_id` = %s
                                    """ % (categoryNum, subCategoryNum)
                                    cursor.execute(query)
                                    columns = cursor.description
                                    tempResult = cursor.fetchall()
                                    if tempResult:
                                        label = tempResult[0][0]
                                    else:
                                        label = "pc_" + categoryNum + "_" + subCategoryNum

                                if i == len(categoriesJson) - 1:
                                    categoriesString += label
                                else:
                                    categoriesString += label + "|"

                    #get rid of extra comma
                    categoriesString = categoriesString[:-1]
                    transformedPayload.append(categoriesString)
                else:
                    transformedPayload.append("")
            elif filterResult == "locations":
                locationJson = jsonString.get("applied_filters").get("locations")
                if locationJson and len(locationJson) > 0:
                    locationString = ""
                    for i, location in enumerate(locationJson):
                        if i == len(locationJson) - 1:
                            locationString += location
                        else:
                            locationString += location + ","

                    transformedPayload.append(locationString)
                else:
                    transformedPayload.append("")
            else:
                filterJson = jsonString.get("applied_filters").get(filterResult)
                if type(filterJson) is dict:
                    filterString = ""
                    if filterJson and len(filterJson) > 0:
                        for i, filters in enumerate(filterJson):
                            if i == len(filterJson) - 1:
                                filterString += filters
                            else:
                                filterString += filters + ","
                    transformedPayload.append(filterString)
                else:
                    if filterJson:
                        transformedPayload.append(filterJson)
                    else:
                        transformedPayload.append("")
        transformedData.append(transformedPayload)

with open('../csv/searchdata.csv', 'w') as f:
    writer = csv.writer(f, delimiter=",")
    writer.writerow(searchColumns)
    for row in transformedData:
        originalId = row[0].split('.')[0]
        row[0] = originalId
        writer.writerow(row)

# disconnect from server
db.close()

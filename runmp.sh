#!/bin/sh
cd /home/csedgwick/db/mp-report/code
/usr/bin/python ./dump.py
/usr/bin/python ./dumpSearch.py
cd ../csv
/usr/local/bin/aws --profile mpreport s3 cp ./mpdata.csv s3://defero-imc-mp-data/
/usr/local/bin/aws --profile mpreport s3 cp ./searchdata.csv s3://defero-imc-mp-data/
